BACKUP_BACKENDS = {
    "Postgres": {
        "BACKEND": "utilites_1c.backup.postgres.BackupBackendPostgres"
    },
    "File": {
        "BACKEND": "utilites_1c.backup.file.BackupBackendFile"
    }
}
